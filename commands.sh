#!/bin/bash
mkdir -p ~/projects/stam/L03/unit-test
code ~/projects/stam/L03/unit-test

git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

echo "node_modules" > .gitignore

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

git remote add origin https://gitlab.com/SanMyl/unit-test
git push -u origin main
import express from 'express';
import calc from './calc.js';

const app = express();
const port = 3000;
const host = "localhost";

// Endpoint - GET http://localhost:3000/
app.get('/', (req, res) => {
    res.status(200).send("Hello world!");
});

// Endpoint - GET http://localhost:3000/add?a=2&b=3
app.get('/add', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const sum = calc.add(a, b);
    res.status(200).send(sum.toString());
});

// Endpoint - GET http://localhost:3000/subtract?a=7&b=5
app.get('/subtract', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const difference = calc.subtract(a, b);
    res.status(200).send(difference.toString());
});

// Endpoint - GET http://localhost:3000/multiply?a=3&b=3
app.get('/multiply', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const product = calc.multiply(a, b);
    res.status(200).send(product.toString());
});

// Endpoint - GET http://localhost:3000/divide?a=6&b=3
app.get('/divide', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const quotient = calc.divide(a, b);
    res.status(200).send(quotient.toString());
});

app.listen(port, host, () => {
    console.log(`http://${host}:${port}`)
});
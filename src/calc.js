/**
 * Adds two numbers together
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b ) => a + b;

const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

const multiply = (multiplier, multiplicand) => {
    return multiplier * multiplicand
};

/**
 * Divides two numbers
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error} 0 division
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("Can't divide by 0");
    const fraction = dividend / divisor;
    return fraction;
};

export default { add, subtract, multiply, divide }